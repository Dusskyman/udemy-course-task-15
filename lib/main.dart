import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:udemy_task_15/pages/main_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Future<FirebaseApp> _initialization = Firebase.initializeApp();
    return FutureBuilder(
      future: _initialization,
      builder: (ctx, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return MaterialApp(
            home: MyHomePage(),
          );
        }
        return MaterialApp(
          home: CircularProgressIndicator(),
        );
      },
    );
  }
}

class MyHomePage extends StatelessWidget {
  UserCredential userCredential;
  void isLoggedIn(BuildContext context) {
    FirebaseAuth.instance.authStateChanges().listen(
      (user) {
        if (user != null) {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (ctx) => MainPage(),
            ),
          );
        }
      },
    );
  }

  singInAnonymously(ctx) async {
    userCredential = await FirebaseAuth.instance.signInAnonymously();
    Navigator.pushReplacement(
      ctx,
      MaterialPageRoute(
        builder: (ctx) => MainPage(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    isLoggedIn(context);
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Colors.grey,
              Colors.blueGrey,
            ],
          ),
        ),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
            color: Colors.blue[100],
          ),
          height: 100,
          width: 200,
          child: InkWell(
            onTap: () => singInAnonymously(context),
            child: Center(child: Text('Log in as Anonymous')),
          ),
        ),
      ),
    );
  }
}
