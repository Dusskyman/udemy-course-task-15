import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:udemy_task_15/main.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            width: double.infinity,
            child: Text(
              'Succsesfully logged in!',
              style: TextStyle(fontSize: 40),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10),
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              color: Colors.blue[100],
            ),
            height: 100,
            width: 200,
            child: InkWell(
              onTap: () {
                FirebaseAuth.instance.signOut().then((_) {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (ctx) => MyHomePage(),
                    ),
                  );
                });
              },
              child: Center(child: Text('Log out')),
            ),
          )
        ],
      ),
    );
  }
}
